package upskill

import scala.annotation.tailrec

object labRecursion:

  def sumFirstNumbers(n: Int) : Long= ???


  def repeatChar(c: Char, n: Int) : String = ???


  def repeatNumber(d: Int, n: Int) : String = ???


  def repeatString(s: String, n: Int) : String = ???

  def factorial(n: Long): Long = ???

  def factorialTR(n: BigInt): BigInt = ???
